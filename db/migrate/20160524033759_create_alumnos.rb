class CreateAlumnos < ActiveRecord::Migration
  def change
    create_table :alumnos do |t|
      t.string :apPaterno
      t.string :apMaterno
      t.string :nombre
      t.string :username
      t.string :password

      t.timestamps null: false
    end
  end
end
