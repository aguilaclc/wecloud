class CreateClases < ActiveRecord::Migration
  def change
    create_table :clases do |t|
      t.string :nombreClase
      t.integer :profesorID

      t.timestamps null: false
    end
  end
end
